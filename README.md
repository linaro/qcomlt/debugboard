QcomLT DebugBoard
=================

[96boards specifications](https://www.96boards.org/specifications/) compliant Mezzanine used to control the DC Input, USB VBUS, PWR/RST buttons and Debug UART via a single USB2.0 CDC interface.

The current design has been saved from KiCad 7.

PDF Schematics are available [here](QcomLT_DebugBoard Schematics.pdf).

Firmware is available at [linaro/qcomlt/debugboard-fw](https://git.codelinaro.org/linaro/qcomlt/debugboard-fw), Gerber zip file in [gerber/](/gerber/) directory and assembly files (BOM and positions) in [assembly/](/assembly/) directory.

![KiCad 3D PCB preview!](/QcomLT_DebugBoard.png)

![KiCad 3D PCB preview, bottom!](/QcomLT_DebugBoard_bottom.png)

License
=======

[CERN-OHL-P](/LICENSE) (CERN Open Hardware Licence v2 permissive)
